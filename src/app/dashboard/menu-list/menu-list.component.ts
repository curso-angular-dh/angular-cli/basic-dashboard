import {Component, OnInit} from '@angular/core';
import {MenuItem} from '../menu-item/menu-item';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.scss']
})
export class MenuListComponent implements OnInit {

  public menu: MenuItem[];

  constructor() {
    this.menu = [];
  }

  ngOnInit() {
    this._buildMenu();
  }

  private _buildMenu(): void {
    this.menu = [
      {
        label: 'Component A',
        route: '/component-a'
      },
      {
        label: 'Component B',
        route: '/component-b'
      }
    ];

  }
}
