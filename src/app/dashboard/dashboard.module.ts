import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AppRoutingModule} from '../app-routing.module';
import {MainComponent} from './main/main.component';
import { HeaderComponent } from './header/header.component';
import { BodyComponent } from './body/body.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { CustomModule } from '../custom/custom.module';

@NgModule({
  declarations: [
    MainComponent,
    HeaderComponent,
    BodyComponent,
    MenuListComponent,
    MenuItemComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    CustomModule
  ],
  exports: [
    MainComponent
  ]
})
export class DashboardModule {
}
