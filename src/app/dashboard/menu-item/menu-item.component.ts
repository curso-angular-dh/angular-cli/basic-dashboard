import {Component, Input, OnInit} from '@angular/core';
import {MenuItem} from './menu-item';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent implements OnInit {

  @Input() menuItem: MenuItem;

  constructor() {
    this.menuItem = new MenuItem('Component A', '/component-a');
  }

  ngOnInit() {
  }

}
