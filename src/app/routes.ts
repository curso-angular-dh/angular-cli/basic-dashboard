import {Routes} from '@angular/router';
import {ComponentAComponent} from './custom/component-a/component-a.component';
import {ComponentBComponent} from './custom/component-b/component-b.component';

export const ROUTES_CONFIG: Routes = [
  {
    path: 'component-a',
    component: ComponentAComponent
  },
  {
    path: 'component-b',
    component: ComponentBComponent
  },
  {
    path: '**',
    redirectTo: 'component-a'
  }
];
